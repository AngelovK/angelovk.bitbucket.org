
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
	var ehrId = "";

    var id1 = "f16f75aa-ba6e-47c6-b08a-20067a96d732";
    var id2 = "dc8e9f52-cff5-470d-be07-41cef92c8bc9";
    var id3 = "f1aba8ba-762b-44db-96f7-b81de310d1e9";
    
    for(var i = 1; i<=3; i++) {
	    if(i == 1) {
	    	$("#izbiraMoznosti").append("<option value="+id1+">Karolina Goceva</option>");
	    }
	    if(i == 2) {
	    	$("#izbiraMoznosti").append("<option value="+id2+">Miki Saseto</option>");
	    }
	    if(i == 3) {
	    	$("#izbiraMoznosti").append("<option value="+id3+">Zoki Sande</option>");
	    }
  
    }
}

function dodajMeritve() {
	
	 sessionId = getSessionId();

	var ehrId = $("#dodajEHR").val();
	var telesnaVisina = $("#dodajVisina").val();
	var telesnaTeza = $("#dodajTeza").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	};
		
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        $("#dodajMeritveSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
		    },
		    error: function(err) {
		    	$("#dodajMeritveSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}


function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();

	if (!ime || !priimek || ime.trim().length == 0 ||
      priimek.trim().length == 0 ) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspesno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#dodajEHR").val(ehrId);
		                    
		                    $("#izbiraMoznosti").append("<option value="+ehrId+">"+ime+" "+priimek+"</option>");
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


	var rez;
	var bmi;
	var teza;
	var visina;
	var gEhrId;


function prikaziMeritve() {

	
	var ehrId = $('#izbiraMoznosti').val();
	$("#podatki1").html("ehrId: "+ehrId+"<br>");
	gEhrId = ehrId;

	var searchData = [{key: "ehrId", value: ehrId}];
$.ajax({
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    success: function (res) {
        
        for (var i in res.parties) {
            var party = res.parties[i];
            $("#podatki2").html("<b>Ime in priimek: "+party.firstNames + ' ' + party.lastNames + "<b><br>");
        }
    }
});

}

function izpisiZnake() {
	
	var sessionId = getSessionId();

	
		$.ajax({
    url: baseUrl + "/view/" + gEhrId + "/weight",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
        if (res.length > 0) {
        	$("#teza").html("Teza: "+res[0].weight+" kg");
			teza =res[0].weight;
			
        }
    }
});    

    $.ajax({
    url: baseUrl + "/view/" + gEhrId + "/height",
    type: 'GET',
    headers: {"Ehr-Session": sessionId},
    success: function (res) {
       if (res.length > 0) {
       	$("#visina").html("Visina: "+res[0].height+" cm");
			visina = res[0].height;
			
		
        }
    }
}); 

}

// google bmi formula

function izracunaj() {
	
        bmi = teza / (visina * visina);

    document.getElementsByClassName("res")[0].innerHTML = Math.round(bmi*10000*10) / 10;
}

function prikaziGraf(){
	$(function () {
	//Better to construct options first and then pass it as a parameter
	var limit =Math.round(bmi*10000*10) / 10;
  	var options = {
		title: {
			text: "Spline Chart using jQuery Plugin"
		},
                animationEnabled: true,
		data: [
		{
			type: "spline", //change it to line, area, column, pie, etc
			dataPoints: [
				{ x: 0, y: 0},
				{ x: limit, y: limit },
				
			]
		}
		]
	};

	 $("#chartContainer").CanvasJSChart(options);

});
}

 function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13,
          mapTypeId: 'roadmap'
        });

        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(25, 25)
            };

            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });
      }



